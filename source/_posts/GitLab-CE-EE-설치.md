---
title: GitLab CE/EE 설치(기존 Apache서버에 설치)
date: 2023-12-15 10:33:07
tags: [gitlab, gitlab-ce, gitlab-ee, apache]
categories:
  - git
---
# GitLab CE/EE 설치

 - 기존 apache서버가 가동중인 우분투20.04 서버에 설치한 내용입니다.
 - [Gitlab공식패키지(Omnibus)](https://about.gitlab.com/install/)를 사용하여 설치합니다.
 - HTTPS적용과 Email(gmail연동)발송까지 다룹니다.

## CE vs EE
 - CE: 무료버전으로 기능제한이 있음(MIT라이선스)
 - EE: 기본적으로 무료사용([GitLab EE라이선스](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/LICENSE)). 필요시 유료기능 업그레이드 가능(추천)
 - 설치방법은 CE와 EE 동일합니다.

## 종속성 설치
```bash
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
```

## 메일링을 위한 postfix 설치
```bash
 sudo apt-get install -y postfix
```

## GitLab 패키지 저장소를 추가 및 설치
 - EXTERNAL_URL은 사용하고자하는 도메인 또는 IP주소를 입력합니다.
 - 특정포트를 사용하는 것도 가능합니다.
```bash
# CE
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ce
# 또는 EE
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
```

## 설치완료
 - 초기 관리자 계정
 - ID: root
 - Password: /etc/gitlab/initial_root_password 파일에 임시 비밀번호가 24시간 동안 저장 되어있습니다.
```bash
cat /etc/gitlab/initial_root_password
```

---
# HTTPS적용

 - apache모듈을 설치/활성화 합니다.(이미 되어있는 경우 생략가능)
 - 도메인 연동(예: gitlab.example.com)
    - gitlab ce/ee는 1218포트로 사용을 예시로 합니다.
 - SLL인증서 적용(certbot 사용)
 - gitlab은 기본 nginx서버를 사용합니다. nginx사용을 비활성화하고 적용합니다.
 
## APACHE 모듈 설치/활성화
 - Reverse Proxy를 사용하여 GitLab ce/ee포트로 연결하기 위해 필요합니다.
 - CSRF Token오류해결을 위해 headers 모듈도 설치합니다.
    ```bash
    # 필요한 Apache 모듈 설치
    sudo apt update
    sudo apt install libapache2-mod-headers libapache2-mod-ssl

    # 모듈 활성화
    sudo a2enmod headers
    sudo a2enmod ssl
    sudo a2enmod proxy
    sudo a2enmod proxy_http
    ```

## APACHE(Reverse Proxy로 1218포트로 연결)
1. /etc/apache2/site-available/gitlab.example.com.conf (가상호스트 파일)을 생성하고 다음과 같이 작성합니다.
    ```bash
    sudo nano /etc/apache2/site-available/gitlab.example.com.conf
    ```
    ```gitlab.example.com.conf
    <VirtualHost *:80>
        ServerAdmin admin@example.com
        ServerName gitlab.example.com
        DocumentRoot "/opt/gitlab/embedded/service/gitlab-rails/public"
        ProxyRequests Off

        <Location />
            ProxyPreserveHost On
            ProxyPass http://0.0.0.0:1218/
            proxyPassReverse http://0.0.0.0:1218/
        </Location>
    RewriteEngine on
    RewriteCond %{DOCUMENT_ROOT}/%{REQUEST_FILENAME} !-f
    RewriteRule .* http://0.0.0.0:1218%{REQUEST_URI} [P,QSA]

    <IfModule mod_headers.c>
        RequestHeader set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}
        RequestHeader set "X-Forwarded-SSL" expr=%{HTTPS}
    </IfModule>
    </VirtualHost>
    ```
    - CSRF Token오류로 인한 로그인 문제해결을 위해 RequestHeader부분을 하단에 추가했습니다.
2. apache에 가상호스트 등록 / apache 재시작
    ```bash
    sudo a2ensite gitlab.example.com.conf
    sudo service apache2 restart
    ```

## SSL 인증서 적용(certbot 으로 let's encrypt 인증서 발급)
1. certbot 설치:
    ```bash
    sudo apt update
    sudo apt install certbot python3-certbot-apache
    ```
2. 인증서 발급
    - 아래명령 실행 후 gitlab.example.com를 선택하여 SSL인증서를 발급받습니다.
    - 이메일주소 입력부분이 있고 나머지는 대부분 동의만 하면됩니다.
    ```bash
    sudo certbot --apache
    ```
3. 인증서 자동갱신(crontab 사용)
    - let's encrypt 인증서는 90일 주기로 갱신이 필요합니다.
    ```
    crontab -e
    
    # 아래 두줄을 추가합니다.
    
    45 3 * * 1 sudo certbot renew --dry-run
    45 3 * * 1 /usr/sbin/service apache2 reload
    ```

---
# E-MAIL발송 설정: Gmail STMP사용하기

## Gmail 앱비밀번호 생성/ 메일POP사용 설정: 따로 다루지 않습니다.

## gitlab.rb파일 설정([문서](https://docs.gitlab.com/omnibus/settings/smtp.html))
1. /etc/gitlab/gitlab.rb의 내용을 수정합니다.
    ```bash
    sudo nano /etc/gitlab/gitlab.rb
    ```

    ```
    external_url "https://gitlab.example.com"

    # 기존내용에서 아래 부분을 추가합니다.

    # nginx비활성화 및 apache연동
    nginx['enable'] = false # nginx 비활성화
    gitlab_workhorse['listen_network'] = "tcp"
    gitlab_workhorse['listen_addr'] = "0.0.0.0:1218" # 1218포트 설정

    # gmail SMTP
    gitlab_rails['smtp_enable'] = true
    gitlab_rails['smtp_address'] = "smtp.gmail.com"
    gitlab_rails['smtp_port'] = 587
    gitlab_rails['smtp_user_name'] = "mygmail@gmail.com" # 구글계정(이메일주소)
    gitlab_rails['smtp_password'] = "#### #### #### ####" # 구글계정 앱 비밀번호
    gitlab_rails['smtp_domain'] = "smtp.gmail.com"
    gitlab_rails['smtp_authentication'] = "login"
    gitlab_rails['smtp_enable_starttls_auto'] = true
    gitlab_rails['smtp_tls'] = false
    gitlab_rails['smtp_openssl_verify_mode'] = 'peer'

    # 이부분은 Google Workspace 사용시 가능(선택)
    # gitlab_rails['gitlab_email_from'] = 'gitlab@example.com' # 발송자 메일주소를 다르게 표시
    # gitlab_rails['gitlab_email_reply_to'] = 'noreply@example.com' # 답변받을 매일주소를 다르게 설정
    ```
2. 저장 후 gitlab설정파일을 다시로드합니다.
    ```
    sudo gitlab-ctl reconfigure
    ```
3. 재시작이 필요하면
    ```
    sudo gitlab-ctl restart
    ```
#### 설정 적용 후 약간의 딜레이가 있는 것 같습니다.(5분이내)