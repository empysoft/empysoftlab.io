---
title: Gitlab/Github에 Hexo블로그 설치
date: 2023-12-04 13:52:36
tags: [hexo, blog, gitlab]
categories:
  - git
---

---
# STEP 1: Gitlab/Github에 프로젝트 생성

## 프로젝트명
 
 프로젝트 이름을 사용자계정 또는 그룹명에 맞게 아래와 같이 설정해야 Gitlab/Github의 서브도메인으로 블로그에 접속할 수 있습니다.

- [계정명].gitlab.io
- [그룹명].gitlab.io

## 빈프로젝트/공개프로젝트로 만들기

- README파일도 생성하지않는 빈프로젝트로 만들어줍니다.
- 모두에게 공개할 예정이면 Public으로 설정.
- (참고) Gitlab의 경우 그룹도메인으로 생성할 경우 그룹자체를 public으로 만들어야 합니다.

---
# STEP 2: Hexo설치(리눅스 예시)

- Git이 설치 되어있어야 합니다.(Git설치 방법은 생략)

## npm으로 Hexo설치

```bash
npm install -g hexo-cli
```

## git으로 deploy하기위한 플러그인 설치(선택)

```bash
npm install hexo-deployer-git --save
```

## 새로운 Hexo 블로그 디렉터리 생성 및 초기화:

Hexo 블로그를 새로운 디렉터리에 초기화합니다.
터미널에서 다음 명령을 사용합니다:

```bash
hexo init myblog
cd myblog
npm install
```

## 로컬에서 Git 초기화:

Hexo 블로그 디렉터리에서 Git을 초기화합니다:

```bash
git init
```

## Gitlab/Github에서 새로운 레포지토리 생성:

STEP1에서 만든 GIT의 레포지토리와 연결합니다.

```bash
git remote add origin [새로운 레포지토리 URL]
```

## GitLab CI/CD 설정:

Hexo 블로그 디렉터리에 .gitlab-ci.yml 파일을 생성하고, 아래의 내용을 추가합니다:
```yaml
image: node:latest

before_script:
  - npm install -g hexo-cli
  - npm install

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```
이 파일은 Hexo 블로그를 빌드하고 결과물을 GitLab Pages에 업로드합니다.

---

## Hexo _config.yml 수정:

Hexo 블로그 디렉터리에 있는 _config.yml 파일을 열어서 url 값을 설정합니다:
```yaml
url: https://사용자명.gitlab.io
```

## 로컬에서 블로그 미리보기 및 업데이트 확인:

Hexo 서버를 실행하여 변경 사항을 로컬에서 확인합니다:
```bash
hexo server
```

## 테스트 글 작성

```bash
hexo new post '새로운 글'
```

## 변경된 내용을 기반으로 블로그를 업데이트합니다:

```bash
hexo generate
hexo deploy
```
또는
```
hexo g -d
```

## Git에 변경사항 커밋 및 푸시

위에서 deploy가 안될경우 ```hexo g``` 로 빌드만 하고 직접 commit & push합니다.

```bash
git add .
git commit -m "블로그 초기화 및 업데이트"
git push -u origin master
```

---
이제 GitLab에 있는 프로젝트도 초기화되었고, 새로운 Hexo 블로그가 설정되어 다시 시작됩니다. 이를 통해 새로운 프로젝트로부터 Hexo 블로그를 다시 설정하고 시작할 수 있습니다.

---
# 참고

- [공식페이지](https://hexo.io/ko/docs/)
- [블로그글](https://futurecreator.github.io/2016/06/14/get-started-with-hexo/)




