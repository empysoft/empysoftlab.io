---
title: PYPI SERVER 구축
date: 2021-5-10 11:21:29
tags: [python pypi service, pypi, ubuntu]
categories:
  - linux
---

## PyPi란?
Python Package Index (PyPI)는 Python 프로그래밍 언어 용 소프트웨어(패키지) 저장소입니다. 본 포스트에서는 pip의 private 저장소를 우분투서버에 설치하는 방법을 정리했습니다.

##  pypi-server 설치하기
```bash
pip install pypiserver
```

## 계정/암호 생성
```bash
# htpasswd사용을 위한 apache2 패키지 설치
sudo apt update
sudo apt install apache2

# 암호화된 파일을 사용하기위한 passlib 패키지 설치
pip install passlib

# 암호생성
# htpasswd -sc [암호화된 파일&경로] [계정ID]
htpasswd -sc .htpasswd userid
# 비밀번호를 2회 입력해준다. -> 현재경로에 .htpasswd파일이 생성됩니다.
```

## PyPi 서버 실행
패키지가 저장될 경로(./pypi_packages)는 알맞게 수정
```bash
# pypi-server -p [Port] -P [htpasswd파일 경로] [패키지가 저장될 경로] &
pypi-server -p 8888 -P .htpasswd ./pypi_packages &
```

## 사용하기
- 사용하려는 사용자 홈(우분투 기준)에 .pypirc파일을 아래와 같이 작성해 준다.
```txt .pypirc
[distutils]
index-servers =
    local

[local]
repository: http://localhost:8888
username: userid
password: userpassword
```
- 만약에 외부 서버에 도메인을 연동하여 사용중이라면 pip를 사용할때 ```--trusted-host``` 옵션을 붙여 줘야한다.
```bash
# 예
pip install some_lib --trusted-host mypypi_server_domain.com
```
