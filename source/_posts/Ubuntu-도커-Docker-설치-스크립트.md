---
title: Ubuntu 도커(Docker)설치 스크립트
date: 2021-3-10 07:23:52
tags: [ubuntu, Docker, 설치]
categories:
  - linux
---

# 우분투에 도커 설치
오라클 클라우드(OCI)에서 CPU에 따라 APT 저장소가 달라 구분해서 작성해 둡니다.

## AMD(OCI E2 micro 등..)
```bash
sudo apt update -y && sudo apt install nano curl -y \
&& sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y \
&& curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - \
&& sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" -y \
&& sudo apt update -y \
&& sudo apt install docker-ce docker-ce-cli containerd.io -y \
&& COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4) \
&& sudo curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
&& sudo chmod +x /usr/local/bin/docker-compose;
```

## ARM(OCI A1 등..)
```bash
sudo apt update -y && sudo apt install nano curl -y \
&& sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y \
&& curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - \
&& sudo add-apt-repository "deb [arch=arm64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" -y \
&& sudo apt update -y \
&& sudo apt install docker-ce docker-ce-cli containerd.io -y \
&& COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4) \
&& sudo curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
&& sudo chmod +x /usr/local/bin/docker-compose;
```
