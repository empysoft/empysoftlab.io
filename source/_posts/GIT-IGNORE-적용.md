---
title: GIT IGNORE 적용
date: 2022-8-10 14:22:39
tags: [git, gitignore]
categories:
  - git
---

# git ignore 적용하기
- .gitignore파일을 설정해도 적용이 안될 시
```bash
git rm -r --cached .
git add .
git commit -m "Apply .gitignore"
```


