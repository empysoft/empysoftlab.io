---
title: Ubuntu GIT-PROMPT(깃 프롬프트) 설정
date: 2021-3-03 16:22:22
tags: [ubuntu, git, git-prompt]
categories:
  - git
---

# [Git] git-prompt(깃 프롬프트) 설정하기
- 우분투(리눅스)에서 git 상태를 프롬프트(prompt)에 표시해주는 스크립트입니다.
- https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh

## 아래는 .bashrc까지 등록해서 자동으로 적용되는 스크립트입니다.(export부분은 취향에 맞게 수정)
```bash
cd ~ \
&& wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -O .git-prompt.sh \
&& echo -e "\nsource ~/.git-prompt.sh" >> .bashrc \
&& echo -e "export PS1='\[\033[0;32m\]\[\033[0m\033[0;32m\]\u\[\033[0;36m\]@\[\033[0;36m\]\h:\w\[\033[0;32m\]\$(__git_ps1)\\\\n\[\033[0;32m\]└─\[\033[0m\033[0;32m\]\$\[\033[0m\033[0;32m\]▶\[\033[0m\] '" >> .bashrc \
&& source .bashrc;
```
