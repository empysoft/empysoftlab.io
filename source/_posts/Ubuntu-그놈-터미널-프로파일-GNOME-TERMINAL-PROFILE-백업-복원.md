---
title: '[Ubuntu] 그놈 터미널 프로파일(GNOME-TERMINAL PROFILE) 백업/복원'
date: 2022-8-10 15:22:39
tags: [linux, ubuntu, 터미널 프로파일, 그놈터미널]
categories:
  - linux
---

# 그놈 터미널(gnome-terminal) 프로파일 백업/복원
- 새로운 PC에 그놈터미널 프로파일 초기설정하는게 귀찮아서 정리합니다.
- 우분투에서 지원하는 기본 터미널인 그놈 터미널의 프로파일은 백업과 불러오기 기능이 지원하지 않아 dconf를 이용해야 합니다.

## 백업(backup)
```bash
dconf dump /org/gnome/terminal/ > gnome_terminal_settings_backup.txt
```

## 복원 전 재설정(삭제) – (실제로 필요하지 않을 수 있음)
```bash
dconf reset -f /org/gnome/terminal/
```

## 복원(restore)
```bash
dconf load /org/gnome/terminal/ < gnome_terminal_settings_backup.txt
```


