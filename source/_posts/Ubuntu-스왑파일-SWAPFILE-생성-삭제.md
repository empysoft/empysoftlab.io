---
title: Ubuntu 스왑파일(SWAPFILE) 생성/삭제
date: 2021-3-03 16:22:22
tags: [ubuntu, 스왑, swap]
categories:
  - linux
---

## 스왑 메모리 확인
```bash
sudo free -m
```
- 위 명령 결과 Swap 라인이 모두 0으로 되어있다면 스왑파일을 생성해 줍니다.
## 스왑 파일 생성
1. swapfile 생성
```bash
#최상위 경로에 swapfile이라는 이름으로 생성
sudo fallocate -l 2G /swapfile
```

2. /swapfile 권한 변경
```bash
sudo chmod 600 /swapfile
```

3. /swapfile을 스왑공간으로 설정
```bash
sudo mkswap /swapfile
```

4. 스왑파일 활성화
```bash
sudo swapon /swapfile
```

## 재부팅 후에도 스왑파일 자동으로 적용하기
1. /etc/fstab 파일 수정
```bash
sudo nano /etc/fstab
```
```
/swapfile swap swap defaults 0 0
# 위 내용을 추가 후 저장
```
2. 재부팅 후에 sudo free -m으로 스왑파일이 적용 되었는지 확인

## 스왑파일 삭제
1. 스왑파일 비활성화
```bash
sudo swapoff -v /swapfile
```

2. /etc/fstab 파일 수정
```bash
sudo nano /etc/fstab
```
```
# /swapfile swap swap defaults 0 0
# 위의 내용을 삭제 후 저장
```

3. 스왑파일 삭제: 마지막으로 스왑파일을 삭제 해주면 완료됩니다.
```bash
sudo rm /swapfile
```
