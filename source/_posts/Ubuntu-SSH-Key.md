---
title: Ubuntu SSH Key
date: 2022-8-10 17:22:39
tags: [linux, ubuntu, ssh, ssh key]
categories:
  - linux
---

# Ubuntu: SSH Key 생성

## ssh-key 생성
```
ssh-keygen -t sra
```

## 원격서버에 공개키 전송
```
scp ~/.ssh/id_rsa.pub ubuntu@[IP_ADDRESS]:~/
```

## 원격 서버에서 공개키 등록
```
ssh ubuntu@[IP_ADDRESS]

#.ssh폴더가 없으면 만들어줍니다.(있으면 생략)
mkdir 700 .ssh

cat ~/id_rsa.pub >> ~/.ssh/authorized_keys
```