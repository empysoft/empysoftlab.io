---
title: 우분투 안드로이드 미러링(SCRCPY)
date: 2022-8-10 16:22:39
tags: [linux, ubuntu, scrcpy, 미러링, android, 안드로이드]
categories:
  - linux
---
## scrcpy 설치
```
sudo apt install android-tools-adb
```

```
sudo snap install scrcpy
```

## 스마트폰 개발자 모드 on
## PC와 스마트폰 USB로 연결
## scrcpy 실행
